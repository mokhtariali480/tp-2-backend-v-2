# Déclaration de la ressource aws_cloudwatch_log_group pour les journaux API Gateway
resource "aws_cloudwatch_log_group" "api_gw" {
  name = "api-gw-logs"  # Nom de votre groupe de journaux CloudWatch
}

# Étape 1 : Création de l'API Gateway
resource "aws_apigatewayv2_api" "job_api_gw" {
  name           = "job_api_gw"
  protocol_type  = "HTTP" 
}

# Étape 2 : Création de la scène "dev" de l'API Gateway
resource "aws_apigatewayv2_stage" "job_api_gw_dev_stage" {
  api_id      = aws_apigatewayv2_api.job_api_gw.id
  name        = "dev"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
    })
  }
}

# Étape 3 : Création de la variable de sortie pour l'URL d'invocation
output "api_gateway_invoke_url" {
  value = aws_apigatewayv2_stage.job_api_gw_dev_stage.invoke_url
}

# Étape 4 : Création de l'intégration avec la lambda "job-api"
resource "aws_apigatewayv2_integration" "job_api_gw_integration" {
  api_id          = aws_apigatewayv2_api.job_api_gw.id
  integration_uri = aws_lambda_function.job_api_lambda.invoke_arn
  integration_type = "AWS_PROXY"
  integration_method = "POST"

  request_parameters = {
    "overwrite:path" = "$request.path"
  }
}

# Étape 5 : Création de la route "ANY /{proxy+}" de l'API Gateway
resource "aws_apigatewayv2_route" "job_api_gw_route" {
  api_id    = aws_apigatewayv2_api.job_api_gw.id
  route_key = "ANY /{proxy+}"
  target    = "integrations/${aws_apigatewayv2_integration.job_api_gw_integration.id}"
}



