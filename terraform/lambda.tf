data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

# resource aws_lambda_function s3_to_sqs_lambda

resource "aws_lambda_function" "s3_to_sqs_lambda" {
  function_name = "s3_to_sqs_lambda"
  handler       = "index.handler"
  memory_size   = 512
  timeout       = 900
  runtime       = "nodejs18.x"
  role          = aws_iam_role.s3_to_sqs_lambda_role.arn
  
  filename      = "/home/ali/Desktop/tp-2-backend-v-2/terraform/s3_to_sqs_lambda.zip"  # Chemin absolu vers le fichier ZIP

  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }
}

# resource aws_lambda_permission allow_bucket

resource "aws_lambda_permission" "allow_s3_invoke" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_job_offer_bucket.arn
}

# resource aws_lambda_function sqs_to_dynamo_lambda

resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  function_name = "sqs_to_dynamo_lambda"
  handler       = "index.handler"
  memory_size   = 512
  timeout       = 30
  runtime       = "nodejs18.x"
  role          = aws_iam_role.sqs_to_dynamo_lambda_role.arn
  filename      = "/home/ali/Desktop/tp-2-backend-v-2/terraform/sqs_to_dynamo_lambda.zip"  # Chemin obsolu vers le fichier ZIP

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job_offers_table.name
    }
  }
}

# resource aws_lambda_permission allow_sqs_queue

resource "aws_lambda_permission" "allow_sqs_invoke" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.job_offers_queue.arn
}

# resource aws_lambda_function job_api_lambda

resource "aws_lambda_function" "job_api_lambda" {
  function_name = "job_api_lambda"
  handler       = "lambda.handler"
  memory_size   = 512
  timeout       = 30
  runtime       = "nodejs18.x"
  filename      = "/home/ali/Desktop/tp-2-backend-v-2/terraform/job_api_lambda.zip"  # Chemin absolu vers le fichier ZIP

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job_offers_table.name
    }
  }

  role = aws_iam_role.job_api_lambda_role.arn
}

# resource aws_lambda_permission allow_api_gw

resource "aws_lambda_permission" "allow_api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.job_api_lambda.function_name
  principal     = "apigateway.amazonaws.com"
}

