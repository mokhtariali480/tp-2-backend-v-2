# resource aws_s3_bucket s3_job_offer_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket        = "seau-job-offer" // Assurez-vous que ce nom est unique globalement
  force_destroy = true
  tags = {
    Name = "job offer"
  }
}

# resource aws_s3_object job_offers

resource "aws_s3_object" "job_offers" {
  depends_on = [aws_s3_bucket.s3_job_offer_bucket]
  bucket     = aws_s3_bucket.s3_job_offer_bucket.id
  key        = "job_offers/"
  content    = "" // Crée un objet S3 vide pour simuler un répertoire
}

# resource aws_s3_bucket_notification bucket_notification

resource "aws_s3_bucket_notification" "s3_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/"
    filter_suffix       = ".csv"
  }

  depends_on = [
    aws_lambda_permission.allow_s3_invoke,
  ]
}

