# IAM Role for Lambda function
resource "aws_iam_role" "s3_to_sqs_lambda_role" {
  name = "s3_to_sqs_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
      },
    ]
  })
}

# IAM Role Policy for S3 access
resource "aws_iam_role_policy" "s3_access" {
  name   = "s3_access_policy"
  role   = aws_iam_role.s3_to_sqs_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "s3:*",
        Resource = "${aws_s3_bucket.s3_job_offer_bucket.arn}/*"
      },
    ]
  })
}

# IAM Role Policy for SQS SendMessage
resource "aws_iam_role_policy" "sqs_send_message" {
  name   = "sqs_send_message_policy"
  role   = aws_iam_role.s3_to_sqs_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "sqs:SendMessage",
        Resource = "${aws_sqs_queue.job_offers_queue.arn}"
      },
    ]
  })
}

resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {
  name = "sqs_to_dynamo_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
      },
    ]
  })
}

resource "aws_iam_role_policy" "sqs_access" {
  name   = "sqs_access_policy"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        Resource = "${aws_sqs_queue.job_offers_queue.arn}"
      },
    ]
  })
}

resource "aws_iam_role_policy" "dynamodb_put" {
  name   = "dynamodb_put_policy"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "dynamodb:PutItem",
        Resource = "${aws_dynamodb_table.job_offers_table.arn}"
      },
    ]
  })
}

resource "aws_iam_role" "job_api_lambda_role" {
  name = "job_api_lambda_role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "job_api_lambda_policy" {
  name        = "job_api_lambda_policy"
  description = "Policy for Lambda to access DynamoDB"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "dynamodb:*",
        Effect   = "Allow",
        Resource = aws_dynamodb_table.job_offers_table.arn
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "job_api_lambda_policy_attachment" {
  policy_arn = aws_iam_policy.job_api_lambda_policy.arn
  role       = aws_iam_role.job_api_lambda_role.name
}



