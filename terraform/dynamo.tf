# resource aws_dynamodb_table job-table

resource "aws_dynamodb_table" "job_offers_table" {
  name           = var.dynamo_job_table_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5

  hash_key = "id"
  range_key = "city"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "city"
    type = "S"
  }

  // Ajoutez des paramètres supplémentaires si nécessaires, comme les tags, les indices secondaires globaux ou locaux, etc.
  // Vous pouvez également ajouter des tags si nécessaire
  tags = {
    Name = "JobOffers"
  }
}

