"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var v1_1 = require("./v1");
var router = (0, express_1.Router)();
router.use('/v1', v1_1.default);
exports.default = router;
