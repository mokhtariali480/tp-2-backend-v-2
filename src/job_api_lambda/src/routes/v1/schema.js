"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.putJob = void 0;
var BaseJoi = require("joi");
var Joi = BaseJoi;
var postJob = Joi.object({
    city: Joi.string().required(),
    company_name: Joi.string().required(),
    title: Joi.string().required(),
    url: Joi.string(),
    contract_type: Joi.string().required(),
    company_sector: Joi.string(),
    user_experience: Joi.string(),
    keyword: Joi.string(),
    created_date: Joi.string(),
    provider: Joi.string(),
});
exports.putJob = Joi.object({
    body: Joi.object({
        company_name: Joi.string().optional(),
        title: Joi.string().optional(),
        url: Joi.string().optional(),
        contract_type: Joi.string().optional(),
        company_sector: Joi.string().optional(),
        user_experience: Joi.string().optional(),
        keyword: Joi.string().optional(),
        created_date: Joi.string().optional(),
        provider: Joi.string().optional(),
    }).optional(),
});
exports.default = {
    postJob: postJob,
    putJob: exports.putJob
};
