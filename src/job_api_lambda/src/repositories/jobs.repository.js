"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
Object.defineProperty(exports, "__esModule", { value: true });
var typedi_1 = require("typedi");
var client_dynamodb_1 = require("@aws-sdk/client-dynamodb");
var JobsRepository = function () {
    var _classDecorators = [(0, typedi_1.Service)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var JobsRepository = _classThis = /** @class */ (function () {
        function JobsRepository_1() {
            this.tableName = process.env.TABLE_NAME;
            this.dynamoDBClient = new client_dynamodb_1.DynamoDBClient();
            if (!this.tableName) {
                throw new Error("TABLE_NAME must not be undefined");
            }
        }
        JobsRepository_1.prototype.getAll = function () {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var getAllJobsCommand, res;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            getAllJobsCommand = new client_dynamodb_1.ScanCommand({
                                TableName: this.tableName
                            });
                            return [4 /*yield*/, this.dynamoDBClient.send(getAllJobsCommand)];
                        case 1:
                            res = _b.sent();
                            return [2 /*return*/, (_a = res.Items) === null || _a === void 0 ? void 0 : _a.map(function (job) {
                                    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
                                    return ({
                                        id: (_a = job.id) === null || _a === void 0 ? void 0 : _a.S,
                                        city: (_b = job.city) === null || _b === void 0 ? void 0 : _b.S,
                                        company_name: (_c = job.company_name) === null || _c === void 0 ? void 0 : _c.S,
                                        title: (_d = job.title) === null || _d === void 0 ? void 0 : _d.S,
                                        url: (_e = job.url) === null || _e === void 0 ? void 0 : _e.S,
                                        contract_type: (_f = job.contract_type) === null || _f === void 0 ? void 0 : _f.S,
                                        company_sector: (_g = job.company_sector) === null || _g === void 0 ? void 0 : _g.S,
                                        user_experience: (_h = job.user_experience) === null || _h === void 0 ? void 0 : _h.S,
                                        keyword: (_j = job.keyword) === null || _j === void 0 ? void 0 : _j.S,
                                        created_date: (_k = job.created_date) === null || _k === void 0 ? void 0 : _k.S,
                                        provider: (_l = job.provider) === null || _l === void 0 ? void 0 : _l.S,
                                    });
                                })];
                    }
                });
            });
        };
        JobsRepository_1.prototype.getById = function (jobId) {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var getJobByIdCommand, res;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            getJobByIdCommand = new client_dynamodb_1.QueryCommand({
                                TableName: this.tableName,
                                ExpressionAttributeValues: {
                                    ':a': { S: jobId },
                                },
                                KeyConditionExpression: 'id = :a',
                                ProjectionExpression: 'id, city, company_name, title, #a, contract_type, company_sector, user_experience, keyword, created_date, provider',
                                ExpressionAttributeNames: { '#a': 'url' }
                            });
                            return [4 /*yield*/, this.dynamoDBClient.send(getJobByIdCommand)];
                        case 1:
                            res = _b.sent();
                            return [2 /*return*/, (_a = res.Items) === null || _a === void 0 ? void 0 : _a.map(function (job) {
                                    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
                                    return ({
                                        id: (_a = job.id) === null || _a === void 0 ? void 0 : _a.S,
                                        city: (_b = job.city) === null || _b === void 0 ? void 0 : _b.S,
                                        company_name: (_c = job.company_name) === null || _c === void 0 ? void 0 : _c.S,
                                        title: (_d = job.title) === null || _d === void 0 ? void 0 : _d.S,
                                        url: (_e = job.url) === null || _e === void 0 ? void 0 : _e.S,
                                        contract_type: (_f = job.contract_type) === null || _f === void 0 ? void 0 : _f.S,
                                        company_sector: (_g = job.company_sector) === null || _g === void 0 ? void 0 : _g.S,
                                        user_experience: (_h = job.user_experience) === null || _h === void 0 ? void 0 : _h.S,
                                        keyword: (_j = job.keyword) === null || _j === void 0 ? void 0 : _j.S,
                                        created_date: (_k = job.created_date) === null || _k === void 0 ? void 0 : _k.S,
                                        provider: (_l = job.provider) === null || _l === void 0 ? void 0 : _l.S,
                                    });
                                }).shift()];
                    }
                });
            });
        };
        JobsRepository_1.prototype.create = function (jobRequest) {
            return __awaiter(this, void 0, void 0, function () {
                var createJobCommand;
                return __generator(this, function (_a) {
                    createJobCommand = new client_dynamodb_1.PutItemCommand({
                        TableName: this.tableName,
                        Item: {
                            id: { S: jobRequest.id },
                            city: { S: jobRequest.city },
                            company_name: { S: jobRequest.company_name },
                            title: { S: jobRequest.title },
                            url: { S: jobRequest.url },
                            contract_type: { S: jobRequest.contract_type },
                            company_sector: { S: jobRequest.company_sector },
                            user_experience: { S: jobRequest.user_experience },
                            keyword: { S: jobRequest.keyword },
                            created_date: { S: jobRequest.created_date },
                            provider: { S: jobRequest.provider }
                        }
                    });
                    return [2 /*return*/, this.dynamoDBClient.send(createJobCommand)];
                });
            });
        };
        JobsRepository_1.prototype.updateJob = function (job, updatedJob) {
            return __awaiter(this, void 0, void 0, function () {
                var updateJobCommand;
                return __generator(this, function (_a) {
                    updateJobCommand = new client_dynamodb_1.UpdateItemCommand({
                        TableName: this.tableName,
                        Key: {
                            id: {
                                S: job.id,
                            },
                            city: {
                                S: job.city,
                            },
                        },
                        UpdateExpression: 'set #a = :m, #b = :n, #c = :o, #d = :p, #e = :q, #f = :r, #g = :s, #h = :t, #i = u',
                        ExpressionAttributeNames: {
                            '#a': 'company_name',
                            '#b': 'title',
                            '#c': 'url',
                            '#d': 'contract_type',
                            '#e': 'company_sector',
                            '#f': 'user_experience',
                            '#g': 'keyword',
                            '#h': 'created_date',
                            '#i': 'provider',
                        },
                        ExpressionAttributeValues: {
                            ':m': { S: updatedJob.company_name ? updatedJob.company_name : job.company_name },
                            ':n': { S: updatedJob.title ? updatedJob.title : job.title },
                            ':o': { S: updatedJob.url ? updatedJob.url : job.url },
                            ':p': { S: updatedJob.contract_type ? updatedJob.contract_type : job.contract_type },
                            ':q': { S: updatedJob.company_sector ? updatedJob.company_sector : job.company_sector },
                            ':r': { S: updatedJob.user_experience ? updatedJob.user_experience : job.user_experience },
                            ':s': { S: updatedJob.keyword ? updatedJob.keyword : job.keyword },
                            ':t': { S: updatedJob.created_date ? updatedJob.created_date : job.created_date },
                            ':u': { S: updatedJob.provider ? updatedJob.provider : job.provider },
                        },
                    });
                    return [2 /*return*/, this.dynamoDBClient.send(updateJobCommand)];
                });
            });
        };
        JobsRepository_1.prototype.deleteJob = function (job) {
            return __awaiter(this, void 0, void 0, function () {
                var deleteJobCommand;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            deleteJobCommand = new client_dynamodb_1.DeleteItemCommand({
                                TableName: this.tableName,
                                Key: {
                                    id: {
                                        S: job.id,
                                    },
                                    city: {
                                        S: job.city,
                                    },
                                },
                            });
                            return [4 /*yield*/, this.dynamoDBClient.send(deleteJobCommand)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        return JobsRepository_1;
    }());
    __setFunctionName(_classThis, "JobsRepository");
    (function () {
        var _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(null) : void 0;
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
        JobsRepository = _classThis = _classDescriptor.value;
        if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return JobsRepository = _classThis;
}();
exports.default = JobsRepository;
