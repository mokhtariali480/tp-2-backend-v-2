import 'reflect-metadata';

import * as express from 'express';
import * as serverless from 'serverless-http';


import routes from './src/routes';

const app = express();

app.use(express.json());

app.use('/', routes);

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(404).send();
});

app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(err.status || 500).send();
});

export const handler = serverless(app);
export default app;
