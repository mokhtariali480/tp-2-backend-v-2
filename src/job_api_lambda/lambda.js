"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
require("reflect-metadata");
var express = require("express");
var serverless = require("serverless-http");
var routes_1 = require("./src/routes");
var app = express();
app.use(express.json());
app.use('/', routes_1.default);
app.use(function (req, res, next) {
    res.status(404).send();
});
app.use(function (err, req, res, next) {
    res.status(err.status || 500).send();
});
exports.handler = serverless(app);
exports.default = app;
